import { DetailJavascriptComponent } from './features/detail/detail-javascript/detail-javascript.component';
import { ListJavascriptComponent } from './features/list/list-javascript/list-javascript.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from './features/detail/detail-css/detail.component';
import { DetailHtmlComponent } from './features/detail/detail-html/detail-html.component';
import { ListComponent } from './features/list/list-css/list.component';
import { ListHTMLComponent } from './features/list/list-html/list-html.component';
import { WelcomeComponent } from './features/welcome/welcome.component';
import { FormSnippetComponent} from './features/form-snippet/form-snippet.component';

const routes: Routes = [
  {path:'', component: WelcomeComponent},
  {path:'CSS', component: ListComponent},
  {path:'HTML', component:ListHTMLComponent},
  {path: 'Javascript', component: ListJavascriptComponent},
  {path:'Welcome', component: WelcomeComponent},
  {path: 'CSS/:id', component: DetailComponent},
  {path: 'HTML/:id', component: DetailHtmlComponent},
  {path: 'Javascript/:id', component: DetailJavascriptComponent},
  {path:'New', component: FormSnippetComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
