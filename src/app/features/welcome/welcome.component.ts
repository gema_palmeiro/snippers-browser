import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  public pageTitle = 'Welcome';
  public introText = 'Choose a language to list the snippets and select which one you want to check';
  constructor() { }

  ngOnInit(): void {
  }

}
