export interface Ilist{
    id : number,
    language : string,
    keywords : string [],
    title : string,
    code : string
}
export interface Idetail{
    title : string,
    code : string
}

export interface Ilanguage{
    title: string,
    language: string,
    id: string
}
