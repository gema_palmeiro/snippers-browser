import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListJavascriptComponent } from './list-javascript.component';

describe('ListJavascriptComponent', () => {
  let component: ListJavascriptComponent;
  let fixture: ComponentFixture<ListJavascriptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListJavascriptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListJavascriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
