import { Component, OnInit } from '@angular/core';
import {Ilanguage} from '../../models/isnippet';
import { SnippetService } from 'src/app/services/snippet.service';

@Component({
  selector: 'app-list-javascript',
  templateUrl: './list-javascript.component.html',
  styleUrls: ['./list-javascript.component.scss']
})
export class ListJavascriptComponent implements OnInit {
  public myList?: Ilanguage[];

  constructor(private snippetService: SnippetService) {}
  ngOnInit(): void {
    this.getListJavascript();
  }
public getListJavascript(): void {
    this.snippetService.getListJavascript().subscribe(
      (data) => {
        this.transformMyList(data);
      }
    );
  }
  private transformMyList(data: any) {
    this.myList = data.map(({title, language, id}: any)=> ({
      title,
      language,
      id
    }));
  }
}
