import { SnippetService } from '../../../services/snippet.service';
import { Component, OnInit } from '@angular/core';
import {Ilanguage} from '../../models/isnippet';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
public myList?: Ilanguage[];

  constructor(private snippetService: SnippetService) {}
  ngOnInit(): void {
    this.getListCSS();
  }
public getListCSS(): void {
    this.snippetService.getListCSS().subscribe(
      (data) => {
        this.transformMyList(data);
      }
    );
  }
  private transformMyList(data: any) {
    this.myList = data.map(({title, language, id}: any)=> ({
      title,
      language,
      id
    }));
  }
}
