import { SnippetService } from '../../../services/snippet.service';
import { Component, OnInit } from '@angular/core';
import {Ilanguage} from '../../models/isnippet';

@Component({
  selector: 'app-list-html',
  templateUrl: './list-html.component.html',
  styleUrls: ['./list-html.component.scss']
})
export class ListHTMLComponent implements OnInit {
  public myList?: Ilanguage[];

  constructor(private snippetService: SnippetService) {}
  ngOnInit(): void {
    this.getListHTML();
  }
public getListHTML(): void {
    this.snippetService.getListHTML().subscribe(
      (data) => {
        this.transformMyList(data);
      }
    );
  }
  private transformMyList(data: any) {
    this.myList = data.map(({title, language, id}: any)=> ({
      title,
      language,
      id
    }));
  }
}
