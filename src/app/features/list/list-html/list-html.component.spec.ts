import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListHTMLComponent } from './list-html.component';

describe('ListHTMLComponent', () => {
  let component: ListHTMLComponent;
  let fixture: ComponentFixture<ListHTMLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListHTMLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListHTMLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
