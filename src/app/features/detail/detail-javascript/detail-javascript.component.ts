import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SnippetService } from 'src/app/services/snippet.service';
import { Idetail } from '../../models/isnippet';

@Component({
  selector: 'app-detail-javascript',
  templateUrl: './detail-javascript.component.html',
  styleUrls: ['./detail-javascript.component.scss']
})
export class DetailJavascriptComponent implements OnInit {
  public snippetID ='';
  public myDetail!: Idetail;
    constructor(private route: ActivatedRoute, private snippetService: SnippetService ) {}
    ngOnInit(){
      this.route.paramMap.subscribe(params =>{
        this.snippetID = params.get('id') as string;
        this.getDetailJavascript();
      });
    }
    public getDetailJavascript(){
      this.snippetService.getDetailJavascript(this.snippetID).subscribe(
        (data)=>{
          this.transformMyDetail(data);
        }
      );
    }
    private transformMyDetail(data: any) {
      this.myDetail = {
        title: data.title,
        code: data.code
      };
    }
}
