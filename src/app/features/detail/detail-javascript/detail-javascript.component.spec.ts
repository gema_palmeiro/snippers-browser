import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailJavascriptComponent } from './detail-javascript.component';

describe('DetailJavascriptComponent', () => {
  let component: DetailJavascriptComponent;
  let fixture: ComponentFixture<DetailJavascriptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailJavascriptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailJavascriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
