import { Idetail } from '../../models/isnippet';
import { SnippetService } from '../../../services/snippet.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
public snippetID ='';
public myDetail!: Idetail;
  constructor(private route: ActivatedRoute, private snippetService: SnippetService ) { }

  ngOnInit(){
    this.route.paramMap.subscribe(params =>{
      this.snippetID = params.get('id') as string;
      this.getDetailCSS(); //obtenemos el id del path de nuestro routing
    });
  }
  public getDetailCSS(){
    this.snippetService.getDetailCSS(this.snippetID).subscribe(
      (data)=>{
        this.transformMyDetail(data);
      }
    );
  }
  private transformMyDetail(data: any) {
    this.myDetail = {
      title: data.title,
      code: data.code
    };
  }
}
