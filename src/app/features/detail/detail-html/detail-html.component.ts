import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Idetail } from '../../models/isnippet';
import { SnippetService } from '../../../services/snippet.service';

@Component({
  selector: 'app-detail-html',
  templateUrl: './detail-html.component.html',
  styleUrls: ['./detail-html.component.scss']
})
export class DetailHtmlComponent implements OnInit {
public snippetID ='';
public myDetail!: Idetail;
  constructor(private route: ActivatedRoute, private snippetService: SnippetService ) { }

  ngOnInit(){
    this.route.paramMap.subscribe(params =>{
      this.snippetID = params.get('id') as string;
      this.getDetailHTML(); //obtenemos el id del path de nuestro routing
    });
  }
  public getDetailHTML(){
    this.snippetService.getDetailHTML(this.snippetID).subscribe(
      (data)=>{
        this.transformMyDetail(data);
      }
    );
  }
  private transformMyDetail(data: any) {
    this.myDetail = {
      title: data.title,
      code: data.code
    };
  }
}

