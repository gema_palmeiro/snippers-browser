import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DetailHtmlComponent } from './detail-html.component';
describe('DetailHtmlComponent', () => {
  let component: DetailHtmlComponent;
  let fixture: ComponentFixture<DetailHtmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailHtmlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailHtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
