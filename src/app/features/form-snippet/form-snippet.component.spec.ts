import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSnippetComponent } from './form-snippet.component';

describe('FormSnippetComponent', () => {
  let component: FormSnippetComponent;
  let fixture: ComponentFixture<FormSnippetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSnippetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSnippetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
