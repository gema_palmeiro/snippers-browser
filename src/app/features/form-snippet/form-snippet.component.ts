import { SnippetService } from './../../services/snippet.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms'

@Component({
  selector: 'app-form-snippet',
  templateUrl: './form-snippet.component.html',
  styleUrls: ['./form-snippet.component.scss']
})
export class FormSnippetComponent implements OnInit {
public newForm: FormGroup;
public submitted: boolean = false; 
constructor(private formBuilder: FormBuilder, private snippetService: SnippetService ) { 
    this.newForm = this.formBuilder.group({
      id:[],
      language:[],
      keywords:[],
      title:[],
      code:[]
    });
  } 

  ngOnInit(): void {
  }
  public onSubmit(){
    this.submitted = true;
    if(this.newForm?.valid){
      const snippet = {
        id: this.newForm.get('id')?.value,
        language: this.newForm.get('language')?.value,
        keywords: this.newForm.get('keywords')?.value,
        title: this.newForm.get('title')?.value,
        code: this.newForm.get('code')?.value,
      }
      this.snippetService.postSnippetCSS(snippet);
    }
    this.newForm?.reset();
    this.submitted = false;
  }
}
