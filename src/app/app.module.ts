
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { ListComponent } from './features/list/list-css/list.component';
import { DetailComponent } from './features/detail/detail-css/detail.component';
import { WelcomeComponent } from './features/welcome/welcome.component';
import { FormSnippetComponent } from './features/form-snippet/form-snippet.component';
import { SnippetService } from './services/snippet.service';
import { ListHTMLComponent } from './features/list/list-html/list-html.component';
import { DetailHtmlComponent } from './features/detail/detail-html/detail-html.component';
import { ListJavascriptComponent } from './features/list/list-javascript/list-javascript.component';
import { DetailJavascriptComponent } from './features/detail/detail-javascript/detail-javascript.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ListComponent,
    DetailComponent,
    WelcomeComponent,
    FormSnippetComponent,
    ListHTMLComponent,
    DetailHtmlComponent,
    ListJavascriptComponent,
    DetailJavascriptComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    HighlightModule
  ],
  providers: [SnippetService,
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        fullLibraryLoader: () => import('highlight.js'),
      }
    }
  ], // Aquí proveemos nuestro servicio al módulo
  bootstrap: [AppComponent]
})
export class AppModule { }
