import { Ilist } from './../features/models/isnippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SnippetService {
  public apiURL = 'http://localhost:3000';
  constructor(private http: HttpClient) {}
  /* CSS */
  getListCSS(): Observable<Ilist[]> {
    return this.http.get(`${this.apiURL}/CSS`).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value unexpected');
        } else {
          console.log(response);
          return response;
        }
      }),
      catchError((error: any) => {
        throw new Error(error.message);
      })
    );
  }
  getDetailCSS(id: string): Observable<Ilist> {
    return this.http.get(`${this.apiURL}/CSS/${id}`).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value unexpected');
        } else {
          return response;
        }
      }),
      catchError((error: any) => {
        throw new Error(error.message);
      })
    );
  }
  public postSnippetCSS(snippet: any) {
    this.http.post(`${this.apiURL}/CSS`,snippet).pipe(
      map((response: any)=>{
        if (!response) {
          throw new Error('Value unexpected');
        } else {
          return response;
        }
      }),
      catchError((error: any) => {
        throw new Error(error.message);
      })
    );
  }
  /* HTML */
  getListHTML(): Observable<Ilist[]> {
    return this.http.get(`${this.apiURL}/HTML`).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value unexpected');
        } else {
          console.log(response);
          return response;
        }
      }),
      catchError((error: any) => {
        throw new Error(error.message);
      })
    );
  }
  getDetailHTML(id: string): Observable<Ilist> {
    return this.http.get(`${this.apiURL}/HTML/${id}`).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value unexpected');
        } else {
          return response;
        }
      }),
      catchError((error: any) => {
        throw new Error(error.message);
      })
    );
  }
  /* JavaScript */
  getListJavascript(): Observable<Ilist[]> {
    return this.http.get(`${this.apiURL}/Javascript`).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value unexpected');
        } else {
          console.log(response);
          return response;
        }
      }),
      catchError((error: any) => {
        throw new Error(error.message);
      })
    );
  }
  getDetailJavascript(id: string): Observable<Ilist> {
    return this.http.get(`${this.apiURL}/Javascript/${id}`).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value unexpected');
        } else {
          return response;
        }
      }),
      catchError((error: any) => {
        throw new Error(error.message);
      })
    );
  }
}
